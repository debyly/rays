use image::ImageBuffer;
use rand::prelude::{thread_rng, Rng};

#[derive(Clone, Debug, Copy)]
struct Camera {
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    origin: Vec3,
}

impl Camera {
    fn new() -> Camera {
        Camera {
            lower_left_corner: Vec3::new(-2.0, -1.0, -1.0),
            horizontal: Vec3::new(4.0, 0.0, 0.0),
            vertical: Vec3::new(0.0, 2.0, 0.0),
            origin: Vec3::new(0.0, 0.0, 0.0),
        }
    }
}

#[derive(Clone, Debug, Copy)]
struct Vec3 {
    x: f32,
    y: f32,
    z: f32,
}

impl Vec3 {
    fn new(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3 { x, y, z }
    }
    // ? should we be generating another one or not?
    fn length(&self) -> f32 {
        (self.x.powf(2.0) + self.y.powf(2.0) + self.z.powf(2.0)).powf(0.5)
    }
    fn squared_length(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }
    fn unit(&self) -> Vec3 {
        let k = 1.0 / (self.length());
        Vec3 {
            z: self.z * k,
            x: self.x * k,
            y: self.y * k,
        }
    }
    fn make_unit(&mut self) {
        let k = 1.0 / (self.length());
        self.x = self.x * k;
        self.y = self.y * k;
        self.z = self.z * k;
    }
    fn subtract(&self, other: Vec3) -> Vec3 {
        Vec3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
    fn div(&self, val: f32) -> Vec3 {
        Vec3 {
            x: self.x / val,
            y: self.y / val,
            z: self.z / val,
        }
    }
    fn multv(&self, v: &Vec3) -> Vec3 {
        Vec3 {
            x: self.x * v.x,
            z: self.z * v.z,
            y: self.y * v.y,
        }
    }
    fn mult(&self, f: f32) -> Vec3 {
        Vec3 {
            x: f * self.x,
            y: f * self.y,
            z: f * self.z,
        }
    }
    fn add_scalar(&self, other: f32) -> Vec3 {
        Vec3 {
            x: self.x + other,
            y: self.y + other,
            z: self.z + other,
        }
    }
    fn add(&self, other: Vec3) -> Vec3 {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

// This is how you 'overload operators' in Rust. In Rust,
// operators are just implementations of the functions in std::ops.
// There is a generic type parameter on the Mul trait, which defines
// the type on the "right hand side" of the operator. It is default to
// the Self type, so if you want to multiply by the same type then you don't
// need to specify it.
//
// There is also the Output associated type which defines what type the result
// of the multiplication will be. In this case, it's also Self (i.e. Vec3).
impl std::ops::Mul for Vec3 {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        self.multv(&other)
    }
}

// This implements the operator Vec3 * f32, and as you can see we use the generic parameter
// I talked about earlier this time.
impl std::ops::Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, other: f32) -> Self {
        self.mult(other)
    }
}
// Finally this implements the operator f32 * Vec3. Note that we are *implementing the Mul
// trait, which is a foreign trate, on a foreign type, f32*. However, because the generic type parameter is a local
// (i.e. defined in this crate) type, this is allowed. It would *not* be allowed to implement
// Mul<f32> for f32, because they are all foreign types and this would violate the orphan rules
// (i.e., what should Rust do if there are multiple implementations of the same trait for the
// same types in different crates? Instead, we just assure that this must not happen).
impl std::ops::Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
        other.mult(self)
    }
}

//?? so does this mean that  we can accomplish various types of  functions with same name taking many different
// kinds of typed arguments?
#[derive(Clone,Debug)]
struct Ray {
    origin: Vec3,
    direction: Vec3,
}

impl Ray {
    fn new(origin: Vec3, direction: Vec3) -> Ray {
        Ray { origin, direction }
    }
    fn point_at_parameter(&self, t: f32) -> Vec3 {
        self.origin.add(self.direction.mult(t))
    }
}

trait Hittable {
    // This function signature is actually one of the most important to illustrate
    // some idioms in Rust. First, we can use `std::ops::Range` to encode a range of
    // values. Second, instad of passing a mutable reference to part of the data that
    // should be returned into the function and then
    // returning only a boolean, we can use the Option type to encode both things
    // into one piece of data.
    fn hit(&self, r: &Ray, t_range: std::ops::Range<f32>) -> Option<HitRecord>;

    // Based ont he above comments, a revised function signature would be:
    // fn hit(&self, r: &Ray, t_range: std::ops::Range<f32>) -> Option<HitRecord>;
    // I'll leave it to you to explore how these changes will impact other parts of the code
    // (of course feel free to ask questions!)
}

struct Sphere {
    center: Vec3,
    r: f32,
}

impl Hittable for Sphere {
    // contents of this were taken from the function circle_hit
    fn hit(&self, r: &Ray, t_range: std::ops::Range<f32>) -> Option<HitRecord> {
        let oc = r.origin.subtract(self.center);
        let a = dot(&r.direction, &r.direction); // b is the direction
                                                 // I think these are off
        let b = 2.0 * dot(&oc, &r.direction);
        let c = dot(&oc, &oc) - self.r * self.r;
        let discriminant = b * b - 4.0 * a * c;
        let mut new_rec = HitRecord::new();
        if discriminant > 0.0 {
            let temp = (-b - discriminant.powf(0.5)) / (2.0 * a); // normally a 2 here but it will cancel
            if t_range.contains(&temp) {
                new_rec.t = temp;
                new_rec.p = r.point_at_parameter(temp);
                new_rec.normal = new_rec.p.subtract(self.center).div(self.r);
                return Some(new_rec);
            }
            // try the other root
            let temp = (-b + discriminant.powf(0.5)) / (2.0 * a);
            if t_range.contains(&temp) {
                new_rec.t = temp;
                new_rec.p = r.point_at_parameter(temp);
                new_rec.normal = (new_rec.p.subtract(self.center)).div(self.r);
                return Some(new_rec);
            }
        }
        None
    }
}

struct HittableList {
    list: Vec<Box<dyn Hittable>>,
}

impl Hittable for HittableList {
    fn hit(&self, r: &Ray, t_range: std::ops::Range<f32>) -> Option<HitRecord> {
        let mut hit_anything = false;
        // mutable range
        let mut mut_range = t_range;
        //?? what should the hit record start as?
        //it gets overwrited each time anyways,
        let mut holder = vec![];
        // ?? how should I handle the propagation of the new hit record here between iterations of the loop?
        for i in 0..self.list.len() {
            let hit_ele = &self.list[i];
            // when do we use map instead of if let?
            if let Some(new_rec) = hit_ele.hit(&r, mut_range.clone()) {
                hit_anything = true;
                mut_range.end = new_rec.clone().t; // should this be new_rec? probably
                holder.push(new_rec);
            }
        }
        holder.pop()
    }
}

// I would make this a method in the impl of Vec,
// i.e. fn dot(&self, other: &Vec3) -> f32 {...}
fn dot(v1: &Vec3, v2: &Vec3) -> f32 {
    v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
}

/// In Rust, we generally want to keep struct names in UpperSnakeCase
/// unless there's a really good reason not to, so I'd just change this to
/// HitRecord.
#[derive(Clone,Debug)]
struct HitRecord {
    t: f32,
    p: Vec3,
    normal: Vec3,
}

impl HitRecord {
    fn new() -> HitRecord {
        HitRecord {
            t: 0.0,
            p: Vec3::new(0.0, 0.0, 0.0),
            normal: Vec3::new(0.0, 0.0, 0.0),
        }
    }
}

//?? can't pass world like this, should I use generics? I really wish rust would let me do this
//kind of thing
//

fn color(r: Ray, world: &dyn Hittable) -> Vec3 {
    // direction is teh direction of the ray
    let mut generator = thread_rng();
    let mut num_hits = 1;
    let mut my_r = r.clone();
    while let Some(rec) = world.hit(&my_r, 0.0001..50000.0) {
        let target = rec.p.add(rec.normal).add_scalar(generator.gen::<f32>());
        my_r = Ray::new(rec.p, target.subtract(rec.p));
        num_hits+=1;
    }
    let u_dir = r.direction.unit();
    let t = 0.5 * (u_dir.y + 1.0);
    Vec3::new(1.0, 1.0, 1.0)
        .mult(1.0 - t)
        .add(Vec3::new(0.5, 0.7, 1.0).mult(t * num_hits as f32))
}

fn main() {
    let nx = 2000;
    let ny = 100;
    let mut im = ImageBuffer::new(nx, ny);
    let cam = Camera::new();
    let ns = 100;
    // create a list of the hittable obs
    let s1 = Sphere {
        center: Vec3::new(0.0, 0.0, -1.0),
        r: 0.5,
    };
    let s2 = Sphere {
        center: Vec3::new(0.0, -100.5, -1.0),
        r: 100.0,
    };
    let world = HittableList {
        list: vec![Box::new(s1), Box::new(s2)],
    };
    let mut generator = thread_rng();
    for j in 0..ny {
        for i in 0..nx {
            println!("i {},j {}",i,j);
            // number of samples
            let mut acc = Vec3::new(0.0, 0.0, 0.0);
            for s in 0..ns {
                // bump the uv slightly to be sub pixel
                let u = (i as f32 + generator.gen::<f32>()) / nx as f32; //generator gives back number between 0 1
                let v = (j as f32 + generator.gen::<f32>()) / ny as f32;
                let ray = Ray::new(
                    cam.origin.clone(), // Since Vec3 is `Copy`, you actually don't need the .clone() here
                    cam.lower_left_corner
                        .add(cam.horizontal.mult(u))
                        .add(cam.vertical.mult(v)),
                );

                let p = ray.point_at_parameter(2.0);
                let col = color(ray, &world);
                acc = acc.add(col);
            }
            //average the color out
            acc = acc.div(ns as f32);
            let pix = image::Rgb([
                (acc.x * 255.0) as u8,
                (acc.y * 255.0) as u8,
                (acc.z * 255.0) as u8,
            ]);
            im.put_pixel(i, j, pix);
        }
    }
    im.save("test.png").expect("saving problem");
}
